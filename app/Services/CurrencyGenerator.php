<?php

namespace App\Services;

class CurrencyGenerator
{
    /**
     * @return Currency[]
     */
    public static function generate()
    {
        $datetime = date('Y-m-d H-i-s');
        $bitcoin = new Currency(1, "Bitcoin", "BTC", 6615.85, $datetime, true);
        $ethereum = new Currency(2, "Ethereum", "ETH", 468.595, $datetime, true);
        $xrp = new Currency(3, "XRP", "XRP", 0.471376, $datetime, true);
        $litecoin = new Currency(4, "Litecoin", "LTC", 81.0976, $datetime, false);

        return [$bitcoin, $ethereum, $xrp, $litecoin];
    }
}