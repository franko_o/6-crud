<?php

namespace App\Services;

/**
 * Class Currency
 * @package App\Services
 */
class Currency
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $short_name;

    /**
     * @var float
     */
    private $actual_course;

    /**
     * @var \DateTime|string
     */
    private $actual_course_date;

    /**
     * @var bool
     */
    private $is_active;

    /**
     * Currency constructor.
     * @param int $id
     * @param string $name
     * @param string $short_name
     * @param float $actual_course
     * @param \DateTime|string $actual_course_date
     * @param bool $is_active
     */
    public function __construct(int $id, string $name, string $short_name,
                                float $actual_course, $actual_course_date, bool $is_active)
    {
        $this->id = $id;
        $this->name = $name;
        $this->short_name = $short_name;
        $this->actual_course = $actual_course;
        $this->actual_course_date = $actual_course_date;
        $this->is_active = $is_active;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->short_name;
    }

    /**
     * @return float
     */
    public function getActualCourse()
    {
        return $this->actual_course;
    }

    /**
     * @return \DateTime|string
     */
    public function getActualCourseDate()
    {
        return $this->actual_course_date;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->is_active;
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'short_name' => $this->getShortName(),
            'actual_course' => $this->getActualCourse(),
            'actual_course_date' => $this->getActualCourseDate(),
            'active' => $this->isActive()
        ];
    }
}