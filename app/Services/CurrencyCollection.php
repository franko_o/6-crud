<?php

namespace App\Services;

class CurrencyCollection implements CurrencyRepositoryInterface
{
    /**
     * @var Currency[]
     */
    private $cryptocurrencies;

    public function __construct()
    {
        $this->cryptocurrencies = CurrencyGenerator::generate();
    }

    public function findAll(): array
    {
        return $this->cryptocurrencies;
    }

    /**
     * @return Currency[]
     */
    public function findActive(): array
    {
        $activeCrypto = [];
        foreach ($this->cryptocurrencies as $crypto) {
            if ($crypto->isActive()) {
                array_push($activeCrypto, $crypto);
            }
        }
        return $activeCrypto;
    }

    /**
     * @param int $id
     * @return Currency|null
     */
    public function findById(int $id): ?Currency
    {
        foreach ($this->cryptocurrencies as $crypto) {
            if ($crypto->getId() == $id) {
                return $crypto;
            }
        }
        return null;
    }

    public function save(Currency $currency): void
    {
        array_push($this->cryptocurrencies, $currency);
    }

    public function delete(Currency $currency): void
    {
        foreach ($this->cryptocurrencies as $key => $cryptocurrency) {
            if ($cryptocurrency->getId() == $currency->getId()) {
                unset($this->cryptocurrencies[$key]);
            }
        }
    }
}