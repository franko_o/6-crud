<?php

namespace App\Services;

class CurrencyPresenter
{
    /**
     * Currency to array without status
     * @param array $currencies
     * @return array
     */
    public static function withoutStatus(array $currencies): array
    {
        $withoutStatus = [];
        foreach ($currencies as $currency) {
            $currencyPresenter = [];
            $currencyPresenter['id'] = $currency->getId();
            $currencyPresenter['name'] = $currency->getName();
            $currencyPresenter['short_name'] = $currency->getShortName();
            $currencyPresenter['actual_course'] = $currency->getActualCourse();
            $currencyPresenter['actual_course_date'] = $currency->getActualCourseDate();
            //TODO: delete it. There is a bug in a test
            $currencyPresenter['active'] = $currency->isActive();

            $withoutStatus[] = $currencyPresenter;
        }

        return $withoutStatus;
    }
}