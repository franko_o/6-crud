<?php

namespace App\Http\Controllers;

use App\Services\Currency;
use App\Services\CurrencyPresenter;
use App\Services\CurrencyRepositoryInterface;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    /**
     * @var CurrencyRepositoryInterface
     */
    private $currencyRepository;

    /**
     * CurrencyController constructor.
     * @param CurrencyRepositoryInterface $currencyRepository
     */
    public function __construct(CurrencyRepositoryInterface $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->getAllCrypto());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // view
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'short_name' => 'required',
            'actual_course' => 'required',
            'actual_course_date' => 'required|string',
            'active' => 'required'
        ]);

        $count = count($this->currencyRepository->findAll());
        $newModel = new Currency(++$count, $request->name, $request->short_name,
            $request->actual_course, $request->actual_course_date, $request->active);
        $this->currencyRepository->save($newModel);

        return response()->json($this->getAllCrypto());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $currency = $this->currencyRepository->findById($id);
        if (null === $currency) {
            abort(404);
        }
        return response()->json($currency->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //view
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currency = $this->currencyRepository->findById($id);
        if (null === $currency) {
            abort(404);
        }

        $name = isset($request->name) ? $request->name : $currency->getName();
        $short_name = isset($request->short_name) ? $request->short_name : $currency->getShortName();
        $actual_course = isset($request->actual_course) ? $request->actual_course : $currency->getActualCourse();
        $actual_course_date = isset($request->actual_course_date) ? $request->actual_course_date : $currency->getActualCourseDate();
        $active = isset($request->active) ? $request->active : $currency->isActive();

        $newModel = new Currency($currency->getId(), $name, $short_name,
            $actual_course, $actual_course_date, $active);
        $this->currencyRepository->delete($currency);
        $this->currencyRepository->save($newModel);

        return response()->json($this->getAllCrypto());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currency = $this->currencyRepository->findById($id);
        if (null === $currency) {
            abort(404);
        }

        $this->currencyRepository->delete($currency);
        return response()->json($this->getAllCrypto());
    }

    /**
     * Get all active currencies
     * @return \Illuminate\Http\JsonResponse
     */
    public function active()
    {
        $activeCurrencies = $this->currencyRepository->findActive();
        $result = CurrencyPresenter::withoutStatus($activeCurrencies);
        return response()->json($result);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        $currencies = $this->currencyRepository->findAll();
        return view('all_currencies', compact('currencies'));
    }

    /**
     * @return array
     */
    protected function getAllCrypto()
    {
        $currencies = $this->currencyRepository->findAll();
        foreach ($currencies as &$currency) {
            $currency = $currency->toArray();
        }
        return $currencies;
    }
}
