@extends('layouts.main')

@section('content')

    <h3>Currencies</h3>
    @foreach($currencies as $currency)
        <div class="currency" id="currency_{{$currency->getId()}}">
            {{$currency->getName()}} : ${{$currency->getActualCourse()}}
        </div>
    @endforeach

@endsection