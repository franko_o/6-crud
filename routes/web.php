<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return 'I deleted it'; });

Route::get('/admin', ['middleware' => 'admin.redirect', function() {return;}]);

Route::prefix('admin')->group(function () {
    Route::get('currencies', 'CurrencyController@getAll');
});